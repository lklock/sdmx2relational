package ec.eurostat.eu.threads;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChunkExecutor {

	private static final Logger logger = LoggerFactory.getLogger(ChunkExecutor.class);
	
	public final static int degree = 4;
	
	@Autowired
    private Function<String, ChunkProcessor> processorFactory;
     	
	//ForkJoinPool threadPool = new ForkJoinPool(ForkJoinPool.commonPool().getParallelism());
	ForkJoinPool threadPool = new ForkJoinPool(degree);
	//ExecutorService threadPool = Executors.newFixedThreadPool(4);
	 	
	 private Future<?> submit(Runnable task) {
		 Future<?> f = threadPool.submit(task);

		 return f;
	 }
	 
	 public Object waitForCompletion(Future<?> f) {
		 Object ret=null;
		 
		  try {
	        	logger.info("Waiting for task " + f.toString() + " to finish");
				ret = f.get();
			} catch (InterruptedException e) {
				logger.error(e.getMessage(), e);
			} catch (ExecutionException e) {
				logger.error(e.getMessage(), e);
			}
		  
		  return ret;
	 }

	public void executeSequentially(String f) {
		logger.info("Accepted chunk for sequential processing " + f);
		
		ChunkProcessor p = processorFactory.apply(f);
		//ChunkProcessor p = new ChunkProcessor(f);
		//DummyProcessor p = new DummyProcessor(f);
		p.run();
		logger.info("Ended Chunk sequential processing " + f);
	}
		
	public Future<?> execParallel(String inputf) { 
		logger.info("Accepted chunk " + inputf);
		ChunkProcessor p = processorFactory.apply(inputf);
		//DummyProcessor p = new DummyProcessor(inputf);
		
		Future<?> f = this.submit(p);
		logger.info(inputf + " submitted to executor");
		
		return f;
	}
}
