package ec.eurostat.eu;

import static org.junit.Assert.*;

import java.util.stream.Stream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ec.eurostat.eu.sdmx.Sdmx2Relational;

public class SdmxRawTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		Stream<String> istream = Sdmx2Relational.chunkFiles.stream();
		istream.map(x -> x.substring(0, x.indexOf(".gz"))).forEach(y -> System.out.println(y));
	}

}
