package ec.eurostat.eu.sdmx;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.eurostat.eu.database.TableManager;

@Component
@Scope("prototype")
public class SdmxStaxParser {

	private final static Logger logger = LoggerFactory.getLogger(SdmxStaxParser.class);
	
	private static final int BATCH_SIZE=1000;
	
	@Autowired
	TableManager tableManager;
	
	XMLInputFactory inputFactory;
	XMLStreamReader reader;
	Connection con=null;
	
	Map<String, String> dimMap = new LinkedHashMap<>();
	Map<String, Observation> obsMap = new LinkedHashMap<>();
	
	int cSeries=0;
	int cObs=0;
	
	public SdmxStaxParser() {
		inputFactory = XMLInputFactory.newInstance();
	}
	
	public void parse(InputStream is, int chunkId) throws XMLStreamException, SQLException {
		int count=0;
		String tempTableName;
		
		long start = System.currentTimeMillis();
		reader = inputFactory.createXMLStreamReader(is);
		
		
		// Must be synchronized across VMs in cluster to avoid concurrency
		synchronized (tableManager) {
			if (!tableManager.hasTable(TableManager.targetTable)) {
				logger.info("Target table does not exist...Creating it");
				tableManager.createTargetTable(Sdmx2Relational.columns, chunkId);
			} else {
				logger.info("Target table already exists...Skipping creation");
				tableManager.addPartition(chunkId);
			}
		}
		
		// Create temporary table for inserting chunk data
		tempTableName = tableManager.createTemporaryTable(chunkId);
		PreparedStatement pstmt = tableManager.getInsertStatement(tempTableName, Sdmx2Relational.columns);

		while (reader.hasNext()) {
			reader.next();
		    if(reader.getEventType() == XMLStreamReader.START_ELEMENT){
		        //System.out.println(reader.getLocalName());
		    	if ("SeriesKey".equals(reader.getLocalName())) {
		    		dimMap.clear();
		    		obsMap.clear();
		    		parseSeriesKey(reader);
		    		//System.out.println(dimMap);
		    	} else if ("Obs".equals(reader.getLocalName())) {
		    		parseObservations(reader);
		    	}
		    }
		    
		    if (reader.getEventType() == XMLStreamReader.END_ELEMENT) {
		    	if ("Series".equals(reader.getLocalName())) {
		    		int nbdims = dimMap.size();
		    		
		    		pstmt.setInt(1, chunkId); // CHUNK ID
		    		pstmt.setInt(2, cSeries); // S_ID
		    		
		    		int dim_seq=0;
		    		for (Map.Entry<String, String> cur : dimMap.entrySet()) {
			    		//pstmt.setString(cur.getKey(), cur.getValue()); // Dimension name, Dimension code
			    		pstmt.setString(6 + dim_seq, cur.getValue());
			    		dim_seq++;
		    		}
		    		
		    		int obs_seq=0;
		    		for (Map.Entry<String, Observation> cur : obsMap.entrySet()) {
			    		pstmt.setString(3, cur.getKey()); // OBS_DIM
			    		Observation curobs = cur.getValue();
			    		pstmt.setString(4, curobs.value); // OBS_VAL
			    		pstmt.setInt(5, obs_seq++);
			    		
			    		//System.out.println("INSERT=" + dimMap.toString() + " obs_seq=" + obs_seq);
			    		//TODO Parse serie attribute
			    		pstmt.setString(6 + nbdims, null); // Serie Attribute
			    		// Obs attributes
			    		if (curobs.attributes.size() > 0) {
			    			pstmt.setString(7 + nbdims, cur.getValue().attributes.toString()); 
			    		} else {
			    			pstmt.setString(7 + nbdims, null);
			    		}
			    		
			    		pstmt.addBatch();
			    		count++;
		    		}
		    	}
		    	
	
		    	// Empty batch
		    	if (count > BATCH_SIZE) {
		    		@SuppressWarnings("unused")
					int []res = pstmt.executeBatch();

		    		if (cObs%100000==0) {
		    			System.out.print('.');
		    		}
		    		count=0;
		    	}
		    	
		    }
		}
		
		System.out.println();
		
		// Empty last batch if there are still any rows to be inserted
		if (count > 0) {
			int []res = pstmt.executeBatch();
		}

		tableManager.commit();
		
		logger.info("Series=" + cSeries + " Obs=" + cObs);
		
		//logger.info("Rows=" + TableManager.count(con));
		pstmt.close();
		con = pstmt.getConnection();
		con.close();
		
		long end = System.currentTimeMillis();
		long usert = (end-start) / 1000;
		logger.info("User time=" + usert);
		
		// Indexing
		for (String cur : Sdmx2Relational.columns) {
			if ("FREQ".equalsIgnoreCase(cur)) {
				logger.info("Skipping index creation on FREQ");
			} else  {
				tableManager.createBitmapIndex(tempTableName, cur);
			}
		}
		
		tableManager.exchangePartition(chunkId, tempTableName);
		tableManager.dropTemporaryTable(chunkId);
	}
	
	
	public void parseSeriesKey(XMLStreamReader reader) throws XMLStreamException {
		boolean processDims=true;
		
        while (processDims && reader.hasNext()) {
            int eventType = reader.next();
			switch (eventType) {
			case XMLStreamReader.START_ELEMENT:
				// <g:Value id="farmtype" value="FT15_SO"/>
				if ("Value".equals(reader.getLocalName())) {
					dimMap.put(reader.getAttributeValue(0), reader.getAttributeValue(1));
				} ; break ;
			case XMLStreamReader.END_ELEMENT:
				if ("SeriesKey".equals(reader.getLocalName())) {
					processDims=false;
				} ; break ;
			}
        }
        
        cSeries++;
	}
	
	public void parseObservations(XMLStreamReader reader) throws XMLStreamException {
		boolean processObs=true;
		String obsDimValue=null;
		Observation obs = new Observation();
		
		while (processObs && reader.hasNext()) {
            int eventType = reader.next();
			switch (eventType) {
			case XMLStreamReader.START_ELEMENT:
				// <g:Value id="farmtype" value="FT15_SO"/>
				if ("ObsDimension".equals(reader.getLocalName())) {
					obsDimValue = reader.getAttributeValue(0);
				} else if ("ObsValue".equals(reader.getLocalName())) {
					if (reader.getAttributeCount()==1) {
						obs.value = reader.getAttributeValue(0);
					} else {
						obs.value = null;
					}
				} else if ("Attributes".equals(reader.getLocalName())) {
					parseAttributes(reader, obs.attributes);
				}
				
				; break ;
			case XMLStreamReader.END_ELEMENT:
				if ("Obs".equals(reader.getLocalName())) {
					processObs=false;
					obsMap.put(obsDimValue,  obs);
					//System.out.println("obsdim=" +  obsDimValue + " obsvalue=" + obs.value + " attr=" + obs.attributes.toString());
					cObs++;
				} ; break ;
			}
        }
	}
	
	public void parseAttributes(XMLStreamReader reader, List<String> lattr) throws XMLStreamException {
		boolean processAttrs=true;
		
		/*
		 <g:Attributes>
         	<g:Value id="OBS_FLAG" value="c" />
       	 </g:Attributes>
		*/
       
        while (processAttrs && reader.hasNext()) {
            int eventType = reader.next();
			switch (eventType) {
			case XMLStreamReader.START_ELEMENT:
				if ("Value".equals(reader.getLocalName())) {
					lattr.add(reader.getAttributeValue(0) + "=" + reader.getAttributeValue(1));
				} ; break ;
			case XMLStreamReader.END_ELEMENT:
				if ("Attributes".equals(reader.getLocalName())) {
					processAttrs=false;
				} ; break ;
			}
        }        
	}
	
	
	public static void main(String []args) {

			File f = new File("D://EUROBASE//farmang_from_api_blob_only1chunk.xml.gz");
			BufferedInputStream bfis=null;
			try {
				GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(f));
				bfis = new BufferedInputStream(gzis);

			} catch (IOException e1) {
				e1.printStackTrace();
			}
			SdmxStaxParser parser = new SdmxStaxParser();
			try {
				parser.parse(bfis, f.getName().hashCode());
			} catch (XMLStreamException | SQLException e) {
				e.printStackTrace();
			}
			try {
				if (bfis!=null) {
					bfis.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
}
