package ec.eurostat.eu.database;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.incrementer.OracleSequenceMaxValueIncrementer;
import org.springframework.stereotype.Repository;

@Repository
public class TableManager {

	private JdbcTemplate jdbcTemplate;

	public final static String targetTable="SQL_NRM_LAURENT";
	
	public final static String tempTablePrefix = "TCHNK_";
	
	private final static Logger logger = LoggerFactory.getLogger(TableManager.class);
	
	private OracleSequenceMaxValueIncrementer chunkSeq; 
	
	private boolean oracleCompress=false;
	
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.chunkSeq = new OracleSequenceMaxValueIncrementer(dataSource, "SDMX_DATA_SET_CHUNK_ID_SEQ");
    }
    
	private void exec(String sql) {
		long start = System.currentTimeMillis();
		logger.info(sql);
		this.jdbcTemplate.execute(sql);
		long end = System.currentTimeMillis();
		long usert = (end-start) / 1000;
		logger.info("User time=" + usert);
	}
		
	public void dropTargetTable() {
		String sql = "DROP TABLE " + targetTable;
		
		try {
			this.exec(sql);
		} catch (DataAccessException e) {
			logger.error(e.getMessage());;
		}

	}
	
	public void createTargetTable(List<String> columns, int initPartition) {
		String sql = "CREATE TABLE " + targetTable + " (" +
				"CHUNK_ID INTEGER, " +
				"S_ID INTEGER," +
				"OBS_DIM VARCHAR2(30)," +
				"OBS_VAL VARCHAR2(30)," +
				"OBS_SEQ INTEGER" ;

		
		for (String cur : columns) {
			sql += "," + cur + " VARCHAR2(30)";
		}
		
		if (initPartition > 0) {
			sql += ",S_ATTR_LIST VARCHAR2(2000), OBS_ATTR_LIST VARCHAR2(2000)) PCTFREE 0 PARTITION BY LIST(CHUNK_ID)";
			sql += " (PARTITION P_" + initPartition + " VALUES(" + initPartition + "))";
		} else {
			sql += ",S_ATTR_LIST VARCHAR2(2000), OBS_ATTR_LIST VARCHAR2(2000)) PCTFREE 0";

		}
		
		if (isOracleCompress()) {
			sql += " COMPRESS";
		}
		
		exec(sql);
		
		// Create local bitmap indexes
		for (String cur : columns) {
			if ("FREQ".equalsIgnoreCase(cur)) {
				logger.info("Skipping initial index creation on FREQ");
			} else  {
				createLocalBitmapIndex(cur);
			}		
		}
	}
	
	public String createTemporaryTable(int chunkId) {
		String temp = TableManager.tempTablePrefix + chunkId;
		String sql = "CREATE TABLE " + temp + " PCTFREE 0 NOLOGGING ";
		if (isOracleCompress()) {
			sql += " COMPRESS ";
		}
		sql += "AS SELECT * FROM " + TableManager.targetTable + " WHERE 1=0";
		exec(sql);
		return temp;
	}
	
	public void dropTemporaryTable(int chunkId) {
		String temp = TableManager.tempTablePrefix + chunkId;
		logger.info("Dropping temporary table for chunk " + chunkId);
		String sql = "DROP TABLE " + temp;
		exec(sql);
	}
	
	public void addPartition(int chunkId) {
		logger.info("Adding partition " + chunkId + " to " + TableManager.targetTable);
		String sql = "ALTER TABLE " + TableManager.targetTable + " ADD PARTITION P_" + chunkId + " VALUES(" + chunkId + ")";
		exec(sql);
	}
	
	public void exchangePartition(int chunkId, String tempTable) {
		logger.info("Exchanging partition " + chunkId + " to " + TableManager.targetTable);
		String sql = "ALTER TABLE " + TableManager.targetTable + " EXCHANGE PARTITION P_" + chunkId + " WITH TABLE " + tempTable;
		exec(sql);
	}
	
	
	public PreparedStatement getInsertStatement(String table, List<String> columns) throws SQLException {
		String sql = "INSERT /*+ APPEND_VALUES */ INTO " + table + " (CHUNK_ID, S_ID, OBS_DIM, OBS_VAL, OBS_SEQ";
		for (String cur : columns) {
			sql += ", " + cur;
		}
		sql += ", S_ATTR_LIST, OBS_ATTR_LIST) VALUES(?, ?, ?, ?, ?";
		for (String cur: columns) {
			sql += ", ?";
		}
		sql += ", ?, ?)";

		logger.info(sql);

		return this.jdbcTemplate.getDataSource().getConnection().prepareStatement(sql);
	}
	
	public int count(Connection con) throws SQLException {
		String sql = "SELECT COUNT(*) FROM " + targetTable;
		
		int cnt = this.jdbcTemplate.queryForObject(sql, Integer.class);
		
		return cnt;
	}
	
	public void commit() {
		exec("COMMIT");
	}
		
	public boolean hasTable(String tableName) {
		final String []type = { "TABLE" };
		Connection con=null;
		boolean ret=false;
		
		try {
			con = this.jdbcTemplate.getDataSource().getConnection(); 
			ResultSet rs = con.getMetaData().getTables(null, null, tableName, type);
			while (rs.next()) {
				ret=true;
				logger.info("Catalog=" + rs.getString(1) + " Schema=" + rs.getString(2) + " Table=" + rs.getString(3));	
			}
			rs.close();
		} catch (SQLException e) {
			DataAccessException de = this.jdbcTemplate.getExceptionTranslator().translate("hasTable", null, e);
			throw de;
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					logger.warn(e.getMessage());
				}
			}
		}
		
		return ret;
	}
	
	public void createBitmapIndex(String tableName, String column) {
		String sql = "CREATE BITMAP INDEX " + tableName + "_" + column + "_B1 ON " + tableName + "(" + column + ")";
		exec(sql);
	}
	
	public void createLocalBitmapIndex(String column) {
		String sql = "CREATE BITMAP INDEX " + targetTable + "_" + column + "_B1 ON " + targetTable + "(" + column + ") LOCAL";
		exec(sql);
	}
	
	/**
	 * @return the next chunk ID from the Oracle sequence SDMX_DATA_SET_CHUNK_ID_SEQ
	 */
	public int getNextChunkId() {
		return chunkSeq.nextIntValue();
	}
	
	/**
	 * Gather table statistics
	 * @param tableName
	 */
	public void gatherStats(String tableName) {
		String sql = "{call dbms_stats.gather_table_stats(" + "ownname => 'DISSCHAIN', " + "tabname => ?, "
				+ "estimate_percent => dbms_stats.auto_sample_size, " + "cascade => dbms_stats.auto_cascade, "
				+ "degree => dbms_stats.auto_degree) }";

		long start = System.currentTimeMillis();
		try {
			logger.info("Gathering statistics for table " + tableName);
			Connection con = this.jdbcTemplate.getDataSource().getConnection();
			CallableStatement callableStatement = con.prepareCall(sql);
			callableStatement.setString(1, tableName);
			callableStatement.executeUpdate();

			con.close();
		} catch (SQLException e) {
			// Don't fail if something goes wrong, statistics will be computed later by automated procedures
			logger.warn(e.getMessage());
		}
		
		long end = System.currentTimeMillis();
		long usert = (end-start) / 1000;
		logger.info("User time=" + usert);
		
		
	}

	public boolean isOracleCompress() {
		return oracleCompress;
	}

	public void setOracleCompress(boolean oracleCompress) {
		this.oracleCompress = oracleCompress;
	}
}
 