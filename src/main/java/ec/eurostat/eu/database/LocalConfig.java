package ec.eurostat.eu.database;

import javax.sql.DataSource;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("local" )
public class LocalConfig {

	public static String localUrl = "jdbc:h2:M://farmang.h2";

	@Bean
	public DataSource dataSource() {
	    return (DataSource) DataSourceBuilder
	        .create()
	        .url(localUrl)
	        .build();
	}
}
