package ec.eurostat.eu.threads;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.util.function.Supplier;
import java.util.zip.GZIPInputStream;

import javax.xml.stream.XMLStreamException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ec.eurostat.eu.database.TableManager;
import ec.eurostat.eu.sdmx.SdmxStaxParser;

@Component
@Scope("prototype")
public class ChunkProcessor implements Runnable {

	private final static Logger logger = LoggerFactory.getLogger(ChunkProcessor.class);
	
	private String inputFile;
	
	@Autowired
    private Supplier<SdmxStaxParser> parserFactory;
	
	@Autowired
	private TableManager tableManager;
	
	public ChunkProcessor(String chunkFile) {
		this.inputFile = chunkFile;
	}
	
	@Override
	public void run() {

		BufferedInputStream bfis=null;
		File f = new File(inputFile);
		
		// Check if we read from a compressed file
		if (inputFile.endsWith(".gz")) {
			try {
				GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(f));
				bfis = new BufferedInputStream(gzis);

			} catch (IOException e) {
				logger.error(e.getMessage(), e);
				return ;
			}
		} else {
			try {
				bfis = new BufferedInputStream(new FileInputStream(f));
			} catch (FileNotFoundException e) {
				logger.error(e.getMessage(), e);
				return ;
			}
		}
		
		if (bfis != null) {
			//SdmxStaxParser parser = new SdmxStaxParser();
			SdmxStaxParser parser = parserFactory.get();

			try {
				int chunkId = tableManager.getNextChunkId();
				parser.parse(bfis, chunkId);
			} catch (XMLStreamException | SQLException e) {
				logger.error(e.getMessage(), e);
			}
			try {
				bfis.close();
			} catch (IOException e) {
				logger.warn(e.getMessage());
			}
		}		
	}

}
