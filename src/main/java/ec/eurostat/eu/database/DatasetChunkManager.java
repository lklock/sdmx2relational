package ec.eurostat.eu.database;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class DatasetChunkManager {

	private JdbcTemplate jdbcTemplate;

	private final static Logger logger = LoggerFactory.getLogger(DatasetChunkManager.class);

	
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
	public void rollbackChunk(int dsId, int chunkId) {
		//TODO
	}
	
	public void linkDataset(int dsId, int prevDsId) {
		// TODO
	}
	
	public void unlinkDataset(int dsId) {
		// TODO
	}

	public void deleteOrphanChunks() {
		// TODO
	}
}
