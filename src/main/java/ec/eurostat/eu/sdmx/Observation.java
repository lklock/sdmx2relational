package ec.eurostat.eu.sdmx;

import java.util.ArrayList;
import java.util.List;

public class Observation {
	String value;
	public List<String> attributes;
	
	public Observation() {
		attributes = new ArrayList<>();
	}
	
	public Observation(String val) {
		value = val;
		attributes = new ArrayList<>();
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		if (value!=null) {
			s.append(value);
		}
		for (String attr : attributes) {
			s.append(':');
			s.append(attr);
		}
		return s.toString();
	}
}
