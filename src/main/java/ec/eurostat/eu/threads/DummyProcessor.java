package ec.eurostat.eu.threads;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DummyProcessor implements Runnable {

	private final static Logger logger = LoggerFactory.getLogger(DummyProcessor.class);
	
	private String inputFile;
	
	public DummyProcessor(String chunkFile) {
		this.inputFile = chunkFile;
	}
	
	@Override
	public void run() {

		logger.info("Starting to process " + inputFile);
		
		File f = new File(inputFile);
		BufferedInputStream bfis=null;
		try {
			GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(f));
			bfis = new BufferedInputStream(gzis);

		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			return ;
		}
		
		byte buf[] = new byte[65536];
		
		try {
			while (bfis.read(buf) > 0) {
				// Just read the file
			}
			
			bfis.close();
		} catch (IOException e) {
			logger.warn(e.getMessage());
		}	
		
		logger.info("Ended processing " + inputFile);
	}
}
