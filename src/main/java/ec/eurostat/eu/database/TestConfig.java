package ec.eurostat.eu.database;
import javax.sql.DataSource;

import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("test")
public class TestConfig {
	
	private static String url = "jdbc:oracle:thin:@//olrdev9.cc.cec.eu.int:1597/DISSCHAIN_ESTAT_04_D_TAF.cc.cec.eu.int";
	private static String user = "DISSCHAIN";
	private static String pwd = "B75u9:29AC";
	
	@Bean
	public DataSource dataSource() {
	    return (DataSource) DataSourceBuilder
	        .create()
	        .username(user)
	        .password(pwd)
	        .url(url)
	        .build();
	}
}
