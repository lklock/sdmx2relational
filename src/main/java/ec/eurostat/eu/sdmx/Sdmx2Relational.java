package ec.eurostat.eu.sdmx;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;

import ec.eurostat.eu.database.TableManager;
import ec.eurostat.eu.threads.ChunkExecutor;
import ec.eurostat.eu.threads.ChunkProcessor;

@SpringBootApplication(scanBasePackages = { "ec.eurostat.eu.threads", "ec.eurostat.eu.sdmx", "ec.eurostat.eu.database",})
public class Sdmx2Relational implements CommandLineRunner {

	private final static Logger logger = LoggerFactory.getLogger(Sdmx2Relational.class);

	public final static List<String> chunkFiles = new ArrayList<String>(Arrays.asList(
			"farmang_chunk_41086.xml.gz", "farmang_chunk_41087.xml.gz", "farmang_chunk_41088.xml.gz",
			"farmang_chunk_41089.xml.gz", "farmang_chunk_41090.xml.gz", "farmang_chunk_41091.xml.gz",
			"farmang_chunk_41092.xml.gz", "farmang_chunk_41093.xml.gz", "farmang_chunk_41094.xml.gz"));

	public final static List<String> single = new ArrayList<String>(Arrays.asList("farmang_from_api_blob_only1chunk.xml.gz"));

	public final static List<String> columns = new ArrayList<String>(Arrays.asList("freq", "age", "sex", 
			"farmtype", "so_eur", "agrarea", "indic_agr", "geo"));
	
	@Autowired
	ChunkExecutor chunkExecutor;
	
	@Autowired
	TableManager tableManager;
	
	public static void main(String[] args) {
        SpringApplication.run(Sdmx2Relational.class, args);
    }

    public void run(String ...args) {
		Options options = new Options();

		options.addOption("seq", false, "Sequential chunk processing");
		options.addOption("par", false, "Parallel chunk processing");
		options.addOption("one", false, "Single file processing");
		options.addOption("sdmxseq", false, "SDMX Uncompressed sequential processing");
		options.addOption("sdmxpar", false, "SDMX Uncompressed parallel processing");
		options.addOption("compress", false, "Use Oracle table compresssion");

		CommandLineParser parser = new DefaultParser();
		CommandLine cmd;
		try {
			cmd = parser.parse(options, args);

			if (cmd.hasOption("seq") && cmd.hasOption("par")) {
				logger.error("-seq and -par options are mutually exclusive");
			}
			
			if (cmd.hasOption("compress")) {
				tableManager.setOracleCompress(true);
			}
			
			Stream<String> istream = Sdmx2Relational.chunkFiles.stream();
			List<Future<?>> lf=null;
			
			if (tableManager.hasTable(TableManager.targetTable)) {
				tableManager.dropTargetTable();
			}
			
			//logger.info("Parallelism degree=" + commonPool.getParallelism()); 
			logger.info("Parallelism degree=" + ChunkExecutor.degree); 
			
			if (cmd.hasOption("seq")) {
				istream.forEach(chunkExecutor::executeSequentially);
			} else if (cmd.hasOption("sdmxseq")) {
				istream.map(x -> x.substring(0, x.indexOf(".gz"))).forEach(chunkExecutor::executeSequentially);
			} else if (cmd.hasOption("par") || cmd.hasOption("sdmxpar")) {
				Function<String, Future<?>> execPar = chunkExecutor::execParallel;
				
				if (cmd.hasOption("par")) {
					lf = istream.sequential().map(execPar).collect(Collectors.toList());
				} else if (cmd.hasOption("sdmxpar")) {
					lf = istream.sequential().map(x -> x.substring(0, x.indexOf(".gz")))
							.map(execPar).collect(Collectors.toList());
				}

				logger.info("Waiting for " + lf.size() + " futures to complete...");
				lf.forEach(f -> {
					try {
						f.get();
					} catch (InterruptedException | ExecutionException e) {
						logger.error(e.getMessage());
					}
				});
				
				logger.info("All tasks completed..");
			} else if (cmd.hasOption("one")) {
				Sdmx2Relational.single.stream().forEach(chunkExecutor::executeSequentially);
			}
			
			tableManager.gatherStats(TableManager.targetTable);
			
		} catch (ParseException e) {
			logger.error(e.getMessage());
		}
    }
	
	@Bean
	public Function<String, ChunkProcessor> chunkProcessorFactory() {
		return x -> chunkProcessorWithParam(x);
	} 
	
	
	@Bean
    @Scope(value = "prototype")
    public ChunkProcessor chunkProcessorWithParam(String file) {
       return new ChunkProcessor(file);
    }
	
	@Bean
	public Supplier<SdmxStaxParser> parserFactory() {
		Supplier<SdmxStaxParser> ret = this::getParserInstance;
		return ret;
	}
	
	@Bean
    @Scope(value = "prototype")
    public SdmxStaxParser getParserInstance() {
       return new SdmxStaxParser();
    }
	
}
